//
//  TabCoordinator.swift
//  Coordinator
//
//  Created by Nikola on 28/02/2018.
//  Copyright © 2018 Ingemark. All rights reserved.
//

import Foundation
import UIKit

class TabCoordinator: NSObject {
    
    var tabBarController: UITabBarController
    var childCoordinators: [NSObject] = []
    
    var homeCoordinator: HomeCoordinator
    var favoriteCoordinator: FavoriteCoordinator
    var cartCoordinator: CartCoordinator
    var cart2Coordinator: CartCoordinator
    var cart3Coordinator:CartCoordinator
    var cart4Coordinator:CartCoordinator
    
    init(tabBarController: UITabBarController) {
        self.tabBarController = tabBarController

        homeCoordinator = HomeCoordinator(navigationController: UINavigationController())
        favoriteCoordinator = FavoriteCoordinator(navigationController: UINavigationController())
        cartCoordinator = CartCoordinator(navigationController: UINavigationController())
        cart2Coordinator = CartCoordinator(navigationController: UINavigationController())
        cart3Coordinator = CartCoordinator(navigationController: UINavigationController())
        cart4Coordinator = CartCoordinator(navigationController: UINavigationController())
        super.init()
    }
    
    func start() {
        
        var controllers: [UIViewController] = []
        
        homeCoordinator.start()
        favoriteCoordinator.start()
        cartCoordinator.start()
        cart2Coordinator.start()
        cart3Coordinator.start()
        cart4Coordinator.start()
        
        let homeController = homeCoordinator.navigationController
        homeController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(), selectedImage: nil)
        childCoordinators.append(homeCoordinator)
        
        let favoriteController = favoriteCoordinator.navigationController
        favoriteController.tabBarItem = UITabBarItem(title: "Favorite", image: UIImage(), selectedImage: nil)
        childCoordinators.append(favoriteCoordinator)
        
        let cartController = cartCoordinator.navigationController
        cartController.tabBarItem = UITabBarItem(title: "Cart", image: UIImage(), selectedImage: nil)
        childCoordinators.append(cartController)
        
        let cart2Controller = cart2Coordinator.navigationController
        cart2Controller.tabBarItem = UITabBarItem(title: "Cart 2", image: UIImage(), selectedImage: nil)
        childCoordinators.append(cart2Controller)
        
        let cart3Controller = cart3Coordinator.navigationController
        cart3Controller.tabBarItem = UITabBarItem(title: "Cart 3", image: UIImage(), selectedImage: nil)
        childCoordinators.append(cart3Controller)
        
        let cart4Controller = cart4Coordinator.navigationController
        cart4Controller.tabBarItem = UITabBarItem(title: "Cart 4", image: UIImage(), selectedImage: nil)
        childCoordinators.append(cart4Controller)
        
        controllers.append(homeController)
        controllers.append(favoriteController)
        controllers.append(cartController)
        controllers.append(cart2Controller)
        controllers.append(cart3Controller)
        controllers.append(cart4Controller)
        
        tabBarController.setViewControllers(controllers, animated: true)
        tabBarController.tabBar.isTranslucent = true
        tabBarController.delegate = self
    }
}

extension TabCoordinator: UITabBarControllerDelegate {
    
}
