//
//  CartCoordinator.swift
//  Coordinator
//
//  Created by Adriagate d.o.o. on 12/11/2018.
//  Copyright © 2018 Ingemark. All rights reserved.
//

import Foundation
import UIKit

class CartCoordinator: NSObject {
    
    var navigationController: UINavigationController
    var childCoordinators: [NSObject] = []
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let cartController = UIStoryboard(name: "Cart", bundle: nil).instantiateInitialViewController() as! CartViewController
        cartController.coordinator = self
        navigationController.setViewControllers([cartController], animated: true)
    }
    
    func pushFavorite() {
        let favoriteController = UIStoryboard(name: "Favorite", bundle: nil).instantiateInitialViewController() as! FavoriteViewController
        //vc.coordinator = self
        navigationController.pushViewController(favoriteController, animated: true)
    }
}
