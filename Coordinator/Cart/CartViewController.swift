//
//  CartViewController.swift
//  Coordinator
//
//  Created by Nikola on 27/02/2018.
//  Copyright © 2018 Ingemark. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    weak var coordinator: CartCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickPush(_ sender: Any) {
        coordinator?.pushFavorite()
    }
    
}
